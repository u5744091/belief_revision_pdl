import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Beleif Revision for Propositional Dynamic Logic
 */
public class BeliefRevision {

    public static final String BELIEF_SET_FILE = "beliefset";
    public static final String BELIEF_SET_OUT_FILE = "beliefset.out";
    public static final String TEMPFILE = "beliefset.temp";
    public static final String SATISFIABILITY_TEST = "./satisfiability_check";
    public static final String PASS = "Formula 1: satisfiable"; /*Satisfiability check pass string*/
    public static final String ERROR = "Error";
    public static final String VAR = "(\\w|\\d)+?";
    public static List<String> formulas;
    private static int counter = 1;
    public static  Clause newBelief;
    public static List<Clause> beliefSet;

    public static void main(String[] args) {
        setupBeliefs(args[0]);
        validityCheck(beliefSet, newBelief);
        beliefRevision(beliefSet, newBelief);
    }

    /**
     * Puts strings to belief set clauses
     * @param newFormula String to transform to clauses
     */
    public static void setupBeliefs(String newFormula){
        formulas = LoadFile.readFile(BELIEF_SET_FILE);
        beliefSet = new ArrayList<>();
        formulas.forEach( s -> beliefSet.add(new Clause(s)) );
        newBelief = new Clause(newFormula);
    }

    /**
     * Check whether the inputs are valid
     * @param beliefSet base belief
     * @param newBelief new belief
     */
    public static void validityCheck(List<Clause> beliefSet, Clause newBelief) {
        List<Clause> tempBelief = copyBeliefs(beliefSet, newBelief);
        String testResult;

        testResult = performSatisfiabilityTest(beliefSet);
        if(testResult.contains(ERROR)){
            System.err.println("Your belief set contains parsing error.");
            System.exit(1);
        }else if(!testResult.contains(PASS)){
            System.err.println("Your initial belief set is not satisfiable");
            System.exit(1);
        }

        testResult = performSatisfiabilityTest(tempBelief);

        if(testResult.contains(ERROR)){
            System.err.println("Invalid belief input");
            System.exit(1);
        } else if (testResult.contains(PASS)){
            return;
        }
        testResult = performSatisfiabilityTest(Arrays.asList(newBelief));
        if(!testResult.contains(PASS)){
            System.err.println("Your new belief is not satisfiable");
            System.exit(1);
        }
    }

    /**
     * Belief revision
     * @param beliefSet base beliefs
     * @param newBelief new belief
     */
    private static void beliefRevision(List<Clause> beliefSet, Clause newBelief) {
        contraction(beliefSet, newBelief);
        output(beliefSet);
    }

    /**
     * Contraction operation and updates beliefset
     * @param beliefSet base beliefs
     * @param newBelief new belief
     */
    private static void contraction(List<Clause> beliefSet, Clause newBelief) {
        List<Clause> tempBelief;
        String impTestResult, satTestResult;
        //Case when no need for contraction
        tempBelief = copyBeliefs(beliefSet);
        tempBelief.add(newBelief);
        satTestResult = performSatisfiabilityTest(tempBelief);
        if(satTestResult.contains(PASS)){
            beliefSet.add(newBelief);
            return;
        }


        //Find a belief set to contract
        List<Clause> relatedBeliefs = findRelation(beliefSet, Arrays.asList(newBelief));
        while( !isFilled(beliefSet) &&  ! (relatedBeliefs.size() == 0)) {
            for (Clause c : relatedBeliefs) {
                if(!c.canRemove()) continue;
                //For formulas that can be combined
                tempBelief = copyBeliefs(beliefSet);
                tempBelief.remove(c);
                if(! c.getVarAfter().containsAll(newBelief.getVarAfter())){
                    Clause concatClause = concatinateBeliefs(c, newBelief);
                    tempBelief.add(concatClause);
                    satTestResult = performSatisfiabilityTest(tempBelief);
                    if(satTestResult.contains(PASS)){
                        beliefSet.remove(c);
                        beliefSet.add(concatClause);
                        return;
                    }

                }
                //For formulas that cannot be combined
                tempBelief = copyBeliefs(beliefSet, newBelief);
                tempBelief.remove(c);
                satTestResult = performSatisfiabilityTest(tempBelief);
                if (satTestResult.contains(PASS)) {
                    beliefSet.remove(c);
                    beliefSet.add(newBelief);
                    return;
                }

            }
            counter++;
            relatedBeliefs = findRelation(beliefSet, relatedBeliefs);
        }

        /* For second loop and after (removing multiple beliefs in a set) */
        for(int i = 1; i < distanceSum(); i++){
            List<List<Clause>> removablesList = findClausesWithSum(i);
            for(List<Clause> removables : removablesList) {
                tempBelief = copyBeliefs(beliefSet);
                tempBelief.removeAll(removables);
                tempBelief.add(newBelief);
                satTestResult = performSatisfiabilityTest(tempBelief);
                if (satTestResult.contains(PASS)) {
                    beliefSet.removeAll(removables);
                    return;
                }
            }
        }
    }

    private static List<List<Clause>> findClausesWithSum(int sum) {
        List<List<Clause>> returningClauses = new ArrayList<>();
        findClausesInner(returningClauses, sum);
        return returningClauses;
    }
    private static void findClausesInner(List<List<Clause>> returnList, int sum){
        for(Clause c: beliefSet) {
            int d = c.getDistance();
            if( d < sum ) findInner(returnList, Arrays.asList(c), sum - d);
        }
    }
    private static void findInner(List<List<Clause>> returnist, List<Clause> tempClauses, int sum){
        if(sum == 0){
            returnist.add(tempClauses);
            return;
        }
        for(Clause c: beliefSet){
            if(!tempClauses.contains(c)){ //if the clause is not part of the list
                int d = c.getDistance();
                if(d < sum) {
                    tempClauses.add(c);
                    findInner(returnist, tempClauses, sum - d);
                    tempClauses.remove(c); // put the clause to the previous state (kind of like a stack)
                }
            }
        }
    }

    public static int distanceSum(){
        int sum = 0;
        for(Clause c: beliefSet){
            sum += c.getDistance();
        }
        return sum;
    }
    /**
     * Combines two beliefs by moving the negation of formula after implication in c1 to before, and adding
     * replacing it with the formula after the implication in c2.
     * c1 : f11 => f12  c2: f21 => f22, then c3 : f11 & ~f12 => f22
     * @param c1 formula to change
     * @param c2 formula to concatinate
     * @return new formula
     */
    public static Clause concatinateBeliefs(Clause c1, Clause c2){
        String s1 = c1.toString();
        String s2 = c2.toString();
        int implyIndex1 = s1.indexOf("=>");
        int implyIndex2 = s2.indexOf("=>");
        String subStringAfter = s1.substring(implyIndex1 + 2).replaceAll("]", "]~").replaceAll("\\s", "");
        String formula = s1.substring(0, implyIndex1 - 1) + "& (" + subStringAfter + ") =>" + s2.substring(implyIndex2 + 2);
        return new Clause(formula);
    }

    /**
     * Checks whether the distance is assigned to all beliefs
     * @param beliefs clauses to be checked
     * @return true if all distance are more than 0, false if there exist a clause with 0 distance.
     */
    public static boolean isFilled(List<Clause> beliefs){
        for(Clause c : beliefs )
            if(c.getDistance() == 0) return false;
        return true;
    }

    /**
     * Finds formulas that are i-adjacent to the base beliefs
     * @param beliefSet beliefs to look from
     * @param baseBeliefs belief to return to
     * @return list of beliefs that are i-adjacent
     */
    public static List<Clause> findRelation(List<Clause> beliefSet, List<Clause> baseBeliefs){
        List<Clause> relatedBeliefs = new ArrayList<>();
        for(Clause c: baseBeliefs) {
            for (String var : c.getVarAfter()) {
                relatedBeliefs.addAll(findSingleRelation(beliefSet, var));
            }
        }
        return relatedBeliefs;
    }

    /**
     * Finds formulas that are i-adjacent to a propositional variable
     * @param beliefSet beliefs to look from
     * @param var varaible to look for
     * @return list of beliefs that are i-adjacent
     */
    public static List<Clause> findSingleRelation(List<Clause> beliefSet, String var){
        List<Clause> relatedBeliefs = new ArrayList<>();
        String fixedVar = var.replaceAll("~", "");

        for (Clause c : beliefSet) {
            if (c.getDistance() == 0 && c.getAllVar().contains(fixedVar)) {
                relatedBeliefs.add(c);
                c.setDistance(counter);
            }
        }
        return relatedBeliefs;
    }

    /**
     * Crates a new list of clause of a list of clauses and a clause.
     * @param beliefSet a list of clause
     * @param belief a clause
     * @return concatination of the two in new allocated memory
     */
    public static List<Clause> copyBeliefs(List<Clause> beliefSet, Clause belief){
        List<Clause> newSet = copyBeliefs(beliefSet);
        newSet.add(belief);
        return newSet;
    }

    /**
     * Creates a copy of list of clauses
     * @param beliefSet a list of clauses
     * @return the copy of the clauses
     */
    public static List<Clause> copyBeliefs(List<Clause> beliefSet){
        List<Clause> newSet = new ArrayList<>();
        newSet.addAll(beliefSet);
        return newSet;
    }

    /**
     * Copies a string to a TEMPFILE = "beliefset.temp"
     * @param s String to add to the test
     */
    private static void prepareTempFile(String s){
        try (BufferedWriter output = new BufferedWriter(new FileWriter(TEMPFILE))) {
            output.write(s);
            output.flush();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Prepares a temp file for a satisfiability test
     * @param beliefSet Clauses for the file
     */
    public static void prepareSatisfiabilityFile(List<Clause> beliefSet){
        prepareTempFile(prepareSatisfiabilityString(beliefSet));
    }

    /**
     * Prepare a temp file for an implication test
     * @param beliefSet Main clause
     * @param belief Clause for implication
     */
    public static void prepareImplicationFile(List<Clause> beliefSet, Clause belief){
        prepareTempFile(prepareImplicationString(beliefSet, belief));
    }

    /**
     * Creates a string for satisfiability test from a belief set
     * @param beliefSet the belief set to transform
     * @return string for satisfiability test
     */
    private static String prepareSatisfiabilityString(List<Clause> beliefSet){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < beliefSet.size(); i++) {
            Clause c = beliefSet.get(i);
            String s = c.toString();
            sb.append(String.format("(%s)", s.replaceAll("\\[", "\\[*")));
            if(i != beliefSet.size() - 1)
                sb.append(" & ");
        }
        return sb.toString();
    }

    /**
     * Creates a string for satisfiability test from a belief set
     * @param beliefSet
     * @param belief
     * @return
     */
    private static String prepareImplicationString(List<Clause> beliefSet, Clause belief){
        StringBuilder sb = new StringBuilder("((");
        String beliefsetString = prepareSatisfiabilityString(beliefSet);
        sb.append(beliefsetString);
        String beliefString = belief.toString().replaceAll("\\[", "\\[*");
        sb.append(") => (")
          .append(beliefString)
          .append(")) & (")
          .append(beliefsetString).append(")"); /*Should look "(( belieset ) => belief) & (beliefset)*/
        return sb.toString();
    }

    /**
     * Performas a satisfiability test
     * @param beliefs
     * @return test result
     */
    public static String performSatisfiabilityTest(List<Clause> beliefs){
        prepareSatisfiabilityFile(beliefs);
        return performTest();
    }

    /**
     * Performas a test for implication
     * @param beliefset Base belief
     * @param belief Belief for implication
     * @return test result
     */
    private static String performImplicationTest(List<Clause> beliefset, Clause belief){
        prepareImplicationFile(beliefset, belief);
        return performTest();
    }

    /**
     * Performs satisfiability test
     * @return test result
     */
    private static String performTest() {
        String resultString = "";
        try {
            Process p = Runtime.getRuntime().exec(SATISFIABILITY_TEST);
            p.waitFor();

            BufferedReader resultReader = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
            String line;
            StringBuilder result = new StringBuilder();
            while ((line = resultReader.readLine()) != null) {
                result.append(line);
            }
            resultString = result.toString();

        } catch (IOException | InterruptedException e1) {
            e1.printStackTrace();
        }
        return resultString;
    }

    /**
     * Writes out the beliefset to out file (beliefset.out)
     * @param beliefSet a belief set ot put in a file
     */
    private static void output(List<Clause> beliefSet) {
        try (BufferedWriter output = new BufferedWriter(new FileWriter(BELIEF_SET_OUT_FILE))) {
            for (Clause c : beliefSet) {
                output.write(String.format("%s\n", c));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
