import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by james on 20/05/16.
 */
public class Clause {
    public static final String VAR = "(\\w|\\d)+";
    public static final Pattern regexPattern = Pattern.compile("(\\[" + VAR + "\\]" + "~?("+ VAR + "))");

    private String formula;
    private String original;
    private int distance = 0;
    private boolean remove = true;
    Set<String> varBefore = new TreeSet<>();

    Set<String> varAfter = new TreeSet<>();
    Boolean removable;

    /**
     * Takes a formula string and assigns variables before and after imply symbol
     * @param equ formula
     */
    public Clause(String equ) {
        original = equ;
        formula = equ.replaceAll("\\s", "");

        int implyIndex = formula.indexOf("=>");
        if(formula.contains("[")) {
        /*Formula with imply symbol*/
            if (implyIndex != -1) {
                Matcher matcherBefore = regexPattern.matcher(formula.substring(0, implyIndex));
                while (matcherBefore.find()) {
                    varBefore.add(matcherBefore.group(3).replaceAll("~", ""));
                }
                Matcher matcherAfter = regexPattern.matcher(formula.substring(implyIndex));
                while (matcherAfter.find()) {
                    varAfter.add(matcherAfter.group(3).replaceAll("~", ""));
                }
            } else {
                Matcher matcher = regexPattern.matcher(formula);
                while (matcher.find()) {
                    varBefore.add(matcher.group(3).replaceAll("~", ""));
                }
            }
        } else {
            remove = false;
            varBefore.add(formula.substring(0, implyIndex - 1));
            varAfter.add(formula.substring(implyIndex + 2).replaceAll("~", ""));
        }
    }


    public void setDistance(int d){
        distance = d;
    }

    public int getDistance(){
        return distance;
    }

    /**
     * Gets variables before imply symbol
     * @return Variables before imply in sets
     */
    public Set<String> getVarBefore(){
        return varBefore;
    }

    public Set<String> getAllVar(){
        Set<String> set = new TreeSet<>();
        set.addAll(getVarBefore());
        set.addAll(getVarAfter());
        return set;
    }

    /**
     * Gets variabales after imply symbol
     * @return variables after imply symbol in sets
     */
    public Set<String> getVarAfter(){
        return varAfter;
    }

    /**
     * Returns string representation of the string which is the original string
     * @return original string
     */
    @Override
    public String toString(){
        return original;
    }

    public boolean canRemove() {return remove;}
}
