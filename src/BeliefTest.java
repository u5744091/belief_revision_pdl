/**
 * Created by james on 2/06/16.
 */
public class BeliefTest {
    public static void main(String[] args) {
        BeliefRevision.setupBeliefs(args[0]);
        BeliefRevision.validityCheck(BeliefRevision.beliefSet, BeliefRevision.newBelief);
        BeliefRevision.beliefSet.add(BeliefRevision.newBelief);
        String result = BeliefRevision.performSatisfiabilityTest(BeliefRevision.beliefSet);
        if(result.contains(BeliefRevision.PASS)){
            System.out.println("Your belief is satisfiable");
        } else {
            System.out.println("Your belief is not satisfiable");
        }
    }
}
