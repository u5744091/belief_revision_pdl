import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 15/05/16.
 */
public class LoadFile {
    public static List<String> readFile(String s){

        List<String> allStrings = new ArrayList<>();

        try(BufferedReader br = new BufferedReader(new FileReader("./" +s))){
            String line = br.readLine();
            while(line != null){
                if(!line.equals("")){
                    allStrings.add(line);
                }
                line = br.readLine();
            }
            return allStrings;

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return null;

    }
}
