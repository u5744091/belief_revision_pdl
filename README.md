## Set up 
Downlaod CPDL satisfiability checker from the following link.

http://users.cecs.anu.edu.au/~rpg/software.html

Add it to the same folder as belief_revision folder.
Run `make`.
## Setting belief set
Put a belief set in a file "filename" and run the following

    ./belief_revision -s "<filename>"
## Performing belief revision
    ./belief_revision "<formula>"
## Test Cases
All test cases can be run with the following command. 

	./runsamples
