SRC = ./src/
BR = $(SRC)BeliefRevision.java
LF = $(SRC)LoadFile.java
C = $(SRC)Clause.java
BT = $(SRC)BeliefTest.java

DEPEND = $(BR) $(LF) $(C)

all: BeliefRevision.class BeliefTest.class

BeliefRevision.class: $(DEPEND)
	javac -d . $(DEPEND)

BeliefTest.class: $(DEPEND) $(BT)
	javac -d . $(DEPEND) $(BT)